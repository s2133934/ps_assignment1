"""
When running from predator_prey:
PYTHONPATH=src:test pytest test/test_unit.py
"""
import unittest
import random
import pytest
import numpy as np
import os
import simulate_predator_prey as source_code
from argparse import ArgumentParser 

def test_calc_hare_density():
    """
    Test that function outputs non-negative arrays if fed a negative array(hs) 
    """
    hs = np.ones((4,4)) * -1
    ps = np.ones((4,4))
    N_ij = np.zeros((4,4),int)
    hs_nu =np.zeros((4,4)) 
    dt,x,y,b,a,r,k = 1,1,1,1,1,1,1

    result = source_code.calc_hare_density(hs_nu,dt,x,y,b,a,r,hs,ps,k,N_ij)
    assert isinstance(result, np.ndarray)
    assert result.all() == np.zeros((4,4)).all() 

def test_calc_puma_density():
    """
    Test that function outputs non-negative arrays if fed a negative array (ps)
    """
    hs = np.ones((4,4))
    ps = np.ones((4,4)) * -1
    N_ij = np.zeros((4,4),int)
    ps_nu =np.zeros((4,4)) 
    dt,x,y,b,m,l = 1,1,1,1,1,1
    result = source_code.calc_puma_density(ps_nu,dt,x,y,b,hs,ps,m,l,N_ij)
    assert isinstance(result, np.ndarray)
    assert result.all() == np.zeros((4,4)).all()

def test_read_in_map():
    """
    Test that the landscape dimensions calculated by the function match the input file
    """
    lfile = '../default_run/map.dat'
    width_halo, width, height_halo, height, lscape = source_code.read_in_map(lfile)

    assert lscape.shape[0] == height_halo, "Height of file does not match the function height"
    assert lscape.shape[1] == width_halo, "Width of file does not match the function height"

def test_read_in_map_halo():
    """
    Test that the function has added a zero value, one cell halo to simulation grid correctly
    """
    lfile = '../default_run/map.dat'
    width_halo, width, height_halo, height, lscape = source_code.read_in_map(lfile)

    assert max(lscape[:,0]) == 0, "Top row halo not zero"
    assert max(lscape[:,-1]) == 0, "Bottom row halo not zero"
    assert max(lscape[0,:]) == 0, "Left side wall not zero"
    assert max(lscape[-1,:]) == 0, "Right side wall not zero"

def test_get_args():
    '''
    Parser test - a SystemExit error should be raised when no map file is given by -f flag
    Test to confirm that this does happen when the wrong arguments are parsed.
    '''
    with pytest.raises(SystemExit):
        parser = source_code.get_args()
    
def test_create_ppm_file():
    """
    Test to confirm that the creation of ppm files is as expected type, size and produces 0 when seed = 0
    """
    seed_grid = np.ones((10,10)) * 0.5
    col_map = np.zeros((10,10))
    max_hs_hp = 0
    x,y=1,1
    
    ppm_file = source_code.create_ppm_file(seed_grid, max_hs_hp,col_map,x,y)
  
    assert isinstance(ppm_file, np.ndarray)
    assert ppm_file.shape[0] == seed_grid.shape[0], "Error: Shape[0] mismatch"
    assert ppm_file.shape[1] == seed_grid.shape[1], "Error: Shape[1] mismatch"
    assert np.isclose(ppm_file[2,2], 0,atol=0.1), "Message!"

def test_create_ppm_cf_oracle():
    """
    For all default parameters, ppm file map_0000.ppm should match oracle map_0000.ppm
    """
    #Run CURRENT code, with ORACLE map.dat and default inputs. Remove ppm's.
    os.system('python src/simulate_predator_prey.py -f ../default_run/map.dat')
    with open('../default_run/map_0000.ppm', 'r') as oracle, open('map_0000.ppm', 'r') as new:
        oracle_file = oracle.read()
        new_file = new.read()
    assert oracle_file == new_file
    os.system('rm *.ppm')

def test_init_seed_grid():
    """
    Test pseudo random seed is carried through and output shape is correct
    """
    random.seed(1)
    seed = random.uniform(0,5.0)
    # NB: dimenisions here DO include a halo
    lscape = np.ones((22,12))
    # NB: Width and height here do not include a halo
    grid_size = source_code.init_seed_grid(seed,lscape,10,20)

    assert np.isclose(seed,0.6718212205620061, atol = 1E-6)
    assert grid_size.shape == lscape.shape

def test_sanity_checker():
    #def sanity_checker(r, a, k, b, m, l):
    r = -0.1
    a = -0.1
    k = -0.1
    b = -0.1
    m = -0.1
    l = -0.1 
    with pytest.raises(Exception):
        result = source_code.sanity_checker(r,a,k,b,m,l)
