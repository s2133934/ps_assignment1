import csv
import numpy as np
import os

def test_default_cf_new_csv():
    """
    For all default parameters, averages.csv should match equivalent oracle file
    """
    #Run CURRENT code, with ORACLE map.dat and default inputs. Remove ppm's.
    os.system('python src/simulate_predator_prey.py -f ../default_run/map.dat')
    os.system('rm *.ppm')
    with open('../default_run/averages.csv', 'r') as oracle, open('averages.csv', 'r') as new:
        oracle_file = oracle.read()
        new_file = new.read()
    assert oracle_file == new_file

def test_default_cf_allwater_csv():
    """
    If no land initialised, the population of all animals should be 0 for all t
    """
    #Run CURRENT code, with all water environment - all populations zero and stay zero. Remove ppm's.
    os.system('python src/simulate_predator_prey.py -f ../default_allwater/map_allwater.dat')
    os.system('rm *.ppm')
    with open('averages.csv', 'r') as results:
        results_var = np.genfromtxt(results, delimiter=',', skip_header=1)
        animals_popn = results_var[:,2:3]
    #Test that the populations of all species should be zero for all t
    assert max(animals_popn) == 0

def test_no_pumas():
    """
    If no pumas, the hare population should be strictly increasing
    """
    os.system('python src/simulate_predator_prey.py -ps 0 -f ../default_run/map.dat')
    os.system('rm *.ppm')
    with open('averages.csv', 'r') as results:
        results_var = np.genfromtxt(results, delimiter=',', skip_header=1)
        puma_popn = results_var[:,3]
        hare_popn = results_var[:,2]
        # to check for strictly increasing list 
        res = all(i < j for i, j in zip(hare_popn, hare_popn[1:])) 
    #Test that the populations of all species should be zero for all t
    assert res == True
    assert max(puma_popn) == 0

def test_no_hares():
    """
    If no hares, the puma population should be strictly decreasing
    """
    os.system('python src/simulate_predator_prey.py -hs 0 -f ../default_run/map.dat')
    os.system('rm *.ppm')
    with open('averages.csv', 'r') as results:
        results_var = np.genfromtxt(results, delimiter=',', skip_header=1)
        puma_popn = results_var[:,3]
        hare_popn = results_var[:,2]
        # to check for strictly increasing list 
        res = all(i > j for i, j in zip(puma_popn, puma_popn[1:])) 
    #Test that the populations of all species should be zero for all t
    assert res == True
    assert max(hare_popn) == 0