"""
Implementation of predator prey model detailed in coursework brief at top level of repository
Main Loop Function: predator_prey -- all other functions are called from within predator_prey
"""

from argparse import ArgumentParser #fn in argparse to return an object with certain properties
import numpy as np
import random
import time
import sys

def get_args():
    """
    Function retrieves arguments from cmdline (or defaults) and returns them
    """
    par = ArgumentParser() #par is an instance of ArgParser
    
    par.add_argument("-r","--birth-hares",    type=float,default=0.08, help="Birth rate of hares")
    par.add_argument("-a","--death-hares",    type=float,default=0.04, help="Rate at which pumas eat hares")
    par.add_argument("-k","--diffusion-hares",type=float,default=0.2,  help="Diffusion rate of hares")
    par.add_argument("-b","--birth-pumas",    type=float,default=0.02, help="Birth rate of pumas")
    par.add_argument("-m","--death-pumas",    type=float,default=0.06, help="Rate at which pumas starve")
    par.add_argument("-l","--diffusion-pumas",type=float,default=0.2,  help="Diffusion rate of pumas")
    par.add_argument("-dt","--delta-t",       type=float,default=0.4,  help="Time step size")
    par.add_argument("-t","--time_step",      type=int,  default=10,   help="Number of time steps at which to output files")
    par.add_argument("-d","--duration",       type=int,  default=500,  help="Time to run the simulation (in timesteps)")
    par.add_argument("-f","--landscape-file", type=str,  required=True,help="Input landscape file")
    par.add_argument("-hs","--hare-seed",     type=int,  default=1,    help="Random seed for initialising hare densities")
    par.add_argument("-ps","--puma-seed",     type=int,  default=1,    help="Random seed for initialising puma densities")
    
    args = par.parse_args()

    r = args.birth_hares
    a = args.death_hares
    k = args.diffusion_hares
    b = args.birth_pumas
    m = args.death_pumas
    l = args.diffusion_pumas
    dt = args.delta_t
    t = args.time_step
    d = args.duration
    lfile = args.landscape_file
    hseed = args.hare_seed
    pseed = args.puma_seed

    # Check r,a,k,b,m,l are not negative (func)
    sanity_checker(r, a, k, b, m, l)

    return r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed

def sanity_checker(r, a, k, b, m, l):
    """
    Check whether variables r,a,k,b,m,l are greater than 0
    """
    for var in (r,a,k,b,m,l):
        if var < 0:
            raise Exception('Variables should be between 0 and 1. Value: {} is not \
                                                 valid for r,a,k,b,m or l'.format(var))

def init_seed_grid(seed,lscape,width,height):
    """
    Function creates the seed population of species, given the landscape (lscape), width
    and height that are created in function 'read_in_map'
    """
    # Create copies of lscape for use initial population
    seed_grid = lscape.astype(float).copy()  
    # Seed pseudo random function
    random.seed(seed)
    # Create uniform population. If lscape or seed = 0, population = 0.
    for x in range(1,height+1):
        for y in range(1,width+1):
            if seed == 0:
                seed_grid[x,y] = 0
            else:
                if lscape[x,y]:
                    seed_grid[x,y] = random.uniform(0,5.0)
                else:
                    seed_grid[x,y] = 0

    return seed_grid

def calc_hare_density(hs_nu,dt,x,y,b,a,r,hs,ps,k,N_ij):
    """
    Calculate hare density of next timestep using equations in brief section 2 (repo top level)
        A few key terms here, see brief for more detail:
            hs / ps = hare / puma seed population
            hs_nu / ps_nu = hare/puma population in next time step
            N_ij = number of neighbouring land squares
    """
    random_walk = k * ( (hs[x-1,y]+hs[x+1,y]+ hs[x,y-1]+hs[x,y+1]) - (N_ij[x,y]*hs[x,y]) )
    predation_rate = (a*hs[x,y]*ps[x,y])
    birth_rate = (r*hs[x,y])

    hs_nu[x,y] = hs[x,y] + dt * ( birth_rate - predation_rate + random_walk )

    if hs_nu[x,y]<0:
        hs_nu[x,y] = 0

    return hs_nu

def calc_puma_density(ps_nu,dt,x,y,b,hs,ps,m,l,N_ij):
    """
    Calculate puma density of next timestep using equations in brief section 2 (repo top level)
        A few key terms here, see brief for more detail:
            hs / ps = hare / puma seed population
            hs_nu / ps_nu = hare/puma population in next time step
            N_ij = number of neighbouring land squares    
    """
    random_walk = l * ( (ps[x-1,y]+ps[x+1,y]+ ps[x,y-1]+ps[x,y+1]) - (N_ij[x,y]*ps[x,y]) )
    birth_rate = (b*hs[x,y]*ps[x,y])
    death_rate = (m*ps[x,y])

    ps_nu[x,y] = ps[x,y] + dt * ( birth_rate - death_rate + random_walk )
    if ps_nu[x,y]<0:
        ps_nu[x,y] = 0
    
    return ps_nu

def create_ppm_file(seed_grid, max_hs_hp,col_map,x,y):
    """
    Creates the .ppm (image output) file for an input seed grid, and pre-initialised col_map.
    """
    if max_hs_hp != 0:
        pixel_colour = (seed_grid[x,y] / max_hs_hp) * 255
    else:
        pixel_colour = 0
    col_map[x-1,y-1] = pixel_colour 

    return col_map

def read_in_map(lfile):
    """
    Read in the specified map file, calculate width and height of grid
    Create landscape by taking fole contents and put a halo of 0 around the edge
    """
    print('print of lfile = ',lfile, type(lfile))
    with open(lfile,"r") as f:
        width,height = [int(i) for i in f.readline().split(" ")]
        print("Width: {} Height: {}".format(width,height))
        #Make a halo of 1 square around the input grid
        width_halo = width + 2 
        height_halo = height + 2 
        lscape = np.zeros((height_halo,width_halo),int) # Init landscape+halo with zeros
        row = 1
        for line in f.readlines():
            values = line.split(" ")
            # Read landscape into array,padding with halo values.
            lscape[row] = [0]+[int(i) for i in values]+[0]
            row += 1

    return width_halo, width, height_halo, height, lscape

def output_loop(hs,ps,n_land,i,dt,height,width,lscape,x,y,h_col_map,p_col_map):
    """
    Creates the output every t timesteps:
    (i) Calculate and (ii) write average to 'averages.csv' (iii) produces ppm files (iv) renders .ppm colours
    """
    # (i) Max H and P density 
    hs_max = np.max(hs)
    hp_max = np.max(ps)
    # (i) Calc. + print averages of pumas and hares if n_dry_lands > 0
    if n_land != 0:              
        hs_av = np.sum(hs) / n_land 
        ps_av = np.sum(ps) / n_land 
    else:
        hs_av = 0                    # average hares = 0, if the num dry squares = 0
        ps_av = 0                    # same for pumas 
    print("Averages. Timestep: {} Time (s): {} Hares: {} Pumas: {}".format(i, i*dt, hs_av, ps_av))
    
    # (ii) Write averages to csv file for current t_step
    with open("averages.csv".format(i),"a") as f: 
        f.write("{},{},{},{}\n".format(i,i*dt,hs_av,ps_av))
    
    # (iii) Creates PPM arrays (func)
    for x in range(1,height+1):
        for y in range(1,width+1):
            if lscape[x,y]:       
                h_col_map = create_ppm_file(hs, hs_max, h_col_map,x,y)
                p_col_map = create_ppm_file(ps, hp_max, p_col_map,x,y)

    # (iv) Open and render the ppm file created 
    with open("map_{:04d}.ppm".format(i),"w") as f:
        header = "P3\n{} {}\n{}\n".format(width,x,255)
        f.write(header)
        for x in range(0,height):
            for y in range(0,width):
                if lscape[x+1,y+1]:  # condition on dry land
                    f.write("{} {} {}\n".format(h_col_map[x,y],p_col_map[x,y],0)) # Set pixel colour
                else:
                    f.write("{} {} {}\n".format(0,0,255)) # Blue pixels (RGB(0,0,255))
    return h_col_map, p_col_map

def predator_prey():
    """
    Main loop of simulation
    """

    # Read in command line args (func)
    r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed = get_args()
    
    #Read in landscape map (func)
    width_halo, width, height_halo, height, lscape = read_in_map(lfile)

    # Count number of land squares (land = 1, water = 0)
    n_land = np.count_nonzero(lscape)
    print("Number of land-only squares: {}".format(n_land))
    
    # Count number of neighbouring land squares ('ij' indicates its iterated over grid as per brief)
    N_ij = np.zeros((height_halo,width_halo),int)
    for x in range(1, height+1):
        for y in range(1, width+1):
            N_ij[x,y] = lscape[x-1,y] + lscape[x+1,y] + lscape[x,y-1] + lscape[x,y+1]

    # Create seed grid, a copy for the next timestep to use and initialise colour output - hares
    hs = init_seed_grid(hseed,lscape,width,height)
    hs_nu = hs.copy()
    h_col_map = np.zeros((height,width),int)
    # Repeat for pumas
    ps = init_seed_grid(pseed,lscape,width,height)
    ps_nu = ps.copy()
    p_col_map = np.zeros((height,width),int)
   
    # Calculate averages of seeded population, where number of neighboring land squares > 0
    if n_land != 0:
        hs_av = np.sum(hs) / n_land
        ps_av = np.sum(ps) / n_land
    else:
        hs_av = 0
        ps_av = 0
    print("Averages. Timestep: {} Time (s): {} Hares: {} Pumas: {}".format(0, 0, hs_av, ps_av))

    # Open averages.csv and write header
    with open("averages.csv","w") as f:
        header = "Timestep,Time,Hares,Pumas\n"
        f.write(header)
    tot_ts = int(d / dt) # Calculate total number of timesteps tot_ts

    # Loop over all timesteps (t = output every t"th" timestep)
    for i in range(0,tot_ts):
        # Find the average num of hares per square, and the max 
        
        if i%t == False: 
            h_col_map, p_col_map = output_loop(hs,ps,n_land,i,dt,height,width,lscape,x,y,h_col_map,p_col_map)

        # Calc new densities for - next iteration (func)
        for x in range(1,height+1):
            for y in range(1,width+1):
                if lscape[x,y]:
                    hs_nu = calc_hare_density(hs_nu,dt,x,y,b,a,r,hs,ps,k,N_ij)
                    ps_nu = calc_puma_density(ps_nu,dt,x,y,b,hs,ps,m,l,N_ij)

        # Swap arrays for next iteration
        hs , hs_nu = hs_nu, hs
        ps, ps_nu = ps_nu, ps

if __name__ == "__main__":
    predator_prey()