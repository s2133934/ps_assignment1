"""
                                              ::: Test Plan :::

############################# Expected Behaviours; test design ideas from Brief: ############################

0)  Test Type: Regression
    Description: Run code with default values as in brief as : python predator_prey/simulate_predator_prey.py -f map.dat
    Conditions for pass: Expected output - run code as is from tar.gz file, for comparison with after refactoring

1)  Test Type: Regression 
    Description: When the landscape is all water, no species survives. Implement by making map.dat a matrix 
        of 0's - if it runs, no species should survive. Run with all default value and 
        map_water.dat: python predator_prey/simulate_predator_prey.py -f map_water.dat
    Conditions for pass: Expected output - all species die pretty quickly (is this time defined or instant? 
        Does it matter?

2)  Test Type: Regression
    Description: All puma and all hares are initialised on land islands, both death rates = 0 and birth rates = 0, 
        their populations should stay constant. Initialise map.dat with islands, 100% of populations of each on islands
    Conditions for pass: Population(hares/pumas)[start time] == population[stop time]

3) Test Type: Regression
    Description: If there are no hares, pumas should die out
    Conditions for pass: constant decline in puma population, constant zero hare population

4)  Test Type: Regression
    Description: If the puma population = 0
    Conditions to pass: Population of pumas remains 0 for all t, and hare population strictly increasing
        (requires visualisation?)

"""

"""
################################ Unit tests: ##############################################

Is is possible to input letters in place of numerical inputs? 
Run input with one or two parameters with 'xx' as their value(s)

Is it possible to put in only some parameters, or do they all need to be in put, or all defaults?
Reference a cell outside of the landscape (index out of range in w and h)
Exception should be thrown if H(x,y) or P(x,y) (respective densities) are negative

Shape of the coloured part of the PPM files should match the shape of the input file with 0 and 1
In Map .dat, feed in values in line 1 that dont match the size of the grid

Testst for nlands, mh and mp !=0 are all present, what happens if I give this negative values 
as this will satisfy the != condition but not be a valid input

###################################Refactoring notes: ###########################################

Repeated code identified in populating the maps with the random seed values for hares and pumas

Redundant loops identified in creation of ppm files and averages

When preparing for next iteration, two/three tmp values specified: Swap arrays for next iteration. x,y = y,x ?


######################## Improvement deas not to be implemented today? ######################################

The code could do with an input deck style file, as opposed to typing in from cmdline 
so that any one parameter can be edited for a sensitivity study without having to type it all in 
and introduce errors potentially

"""

