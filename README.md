# PS_assignment1

Programming Skills Assignment 1: Refactoring and testing

                                .gitlog properties:

Name: s2133934 (this is deliberate)
Email: s2133934@ed.ac.uk
 ------------------------------------------------------------------------------------------
                                Required packages:

python3, numpy, pytest, coverage, random, argparse, sys, unittest, os

------------------------------------------------------------------------------------------
                                RUN INSTRUCTIONS:
TO RUN SOURCE CODE: Navigate to the coursework-predator-prey-refactoring/ and run:
"python3 predator_prey/src/ -f map.dat"

Outputs created:
map_****.ppm (number depends on inputs t and dt - default 125 files)[these are in .gitignore]
averages.csv

TO RUN TESTS: Navigate to the predator_prey/ and run:
"PYTHONPATH=src:test pytest test/test_unit.py"
"PYTHONPATH=src:test pytest test/test_regression.py"

To include coverage reports:
$ PYTHONPATH=src:test coverage run -m pytest test/test_unit.py
$ PYTHONPATH=src:test coverage run -m pytest test/test_regression.py

To commit reports to files:
$coverage report >> test/coverage_report_unit.txt 
$coverage report >> test/coverage_report_regression.txt
---------------------------------------------------------------------------------------------
                             Structure of this repository:

Top level|--->                         <---Lower levels---> 
README.md (this file)
RefactorTestCoursework.pdf
coursework-predator-prey-refactoring/
    README.md (README for original code intact)	
    averages.csv
    map.dat
    default_allwater/
        map_allwater.dat
        simulate_predator_prey.py -->unedited source
        averages.csv --> all water (no land) oracle
    default_run/  
        simulate_predator_prey.py -->unedited source
        average.csv --> the all defaults oracle
        map.dat
    default_island/
        map_islands.dat
        simulate_predator_prey.py --> unedited source
    predator_prey/
        src/
            simulate_predator_prey.py --> refactored and tested course code
            test_rationale.py
        test/
            test_unit.py  --> unit tests
            test_regression.py --> regression tests
    		
---------------------------------------------------------------------------------------------
                                       Test Cases

Special Justification for test cases: (not included in test_file)

(0) A non object-oriented approach was taken consistent with the source code

(1) Landscape restrictions:
I have not tested the case where the landscape is initialised with all sorts of crazy values, like negatives as the README supplied with the code specifies in line 80 that the land = 1 and the water = 0. As this is stated I am trusting this as fact. 

(2) Testing for no -f --landscape_file given:
This is not a test case here as the ArgParser section (a now function 'get_args') already has a required==True flag meaning that the following error is thrown in this instance:

"usage: simulate_predator_prey.py [-h] [-r BIRTH_HARES] [-a DEATH_HARES]
                                 [-k DIFFUSION_HARES] [-b BIRTH_PUMAS]
                                 [-m DEATH_PUMAS] [-l DIFFUSION_PUMAS]
                                 [-dt DELTA_T] [-t TIME_STEP] [-d DURATION] -f
                                 LANDSCAPE_FILE [-hs HARE_SEED]
                                 [-ps PUMA_SEED]
simulate_predator_prey.py: error: the following arguments are required: -f/--landscape-file"

(3) Regression tests for large scale code behaviours are as follows:
    - Default values before refactor = default values after refactor
    - Code behaviour with all water - no species surviving
    - Code behavior with no predation - prey population increases
    - Code behaviour with no prey - predator population decreases

---------------------------------------------------------------------------------------------
                                    Refactoring notes:

The logic ' != 0 ' has been left in the code. I would normally seek to remove this as it does allow for negative cases to pass, which is nonsensical in the context of this set of equations, but could allow for broader uses of this code. In this case, it has been used to prevent division by zero errors.

In the case of generating files with map.dat, the user has been instructed to use only 0 and 1, but other values are allowed. The n_land number is also a count of non-zero squares, which would include all numbers that are not 0, when the only truly valid entry for land is 1. This has been left as it is as this has been made as a clear user responsibility in the original README file.

Calc_hare_density / Calc_puma_density: 
    I have kept short names 'hs', 'ps', 'hs_nu', 'ps_nu', 'N_ij' for these variables as they are used several times in the functions 'calc_hare_density' and 'calc_puma_density' and would be unreadable with longer names. Names are explained at the top of the functions.

    Refactoring of the differential equation, names are consistent with the brief document, e.g.. use of random_walk = "" , and birth_rate and death_rate so that users can easily refer to docs.

Line 149 - line 159 -- there is a clear ability to take this out as a function also, but decided against this as the net benefit would be minimal by the time a function has been defined etc.

------------------------------------------------------------------------------------------------
                                    Improvements:

This code could benefit from the use of a file (input deck) to allow a visual list of arguments to be passed in.

There are more eficient ways of looping over 2D arrays than (for example) - this appears several times
        for x in range(1,height+1):
            for y in range(1,width+1):

I would delete the printing to screen on the basis of performance as this is computationally expensive - could investigate use of a progress monitor instead.